#pragma once

// #include "lexer/token.hpp"
#include "parser/node.hpp"

#include <vector>
#include <string>

namespace Basic
{
  class Basic
  {
  public:
    std::shared_ptr<Node> run(std::string filename, std::string text);
  };
} // namespace Basic
