#pragma once

#include "lexer/position.hpp"

#include <exception>
#include <memory>
#include <sstream>

namespace Basic {
  class Error : public std::exception {
   private:
    std::shared_ptr<Position> start;
    std::shared_ptr<Position> end;
    std::string name;
    std::string details;

   public:
    Error(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string name, std::string details);
    const char* what() const throw ();
  };

  class IllegalCharError : public Error {
   public:
    IllegalCharError(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string details);
  };

  class InvalidSyntaxError : public Error {
   public:
    InvalidSyntaxError(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string details);
  };
}