#pragma once

#include "lexer/position.hpp"

#include <memory>
#include <ostream>
#include <string>

namespace Basic
{
  enum TokenType
  {
    TT_INT,
    TT_FLOAT,
    TT_PLUS,
    TT_MINUS,
    TT_MUL,
    TT_DIV,
    TT_LPAREN,
    TT_RPAREN,
    TT_EOF
  };

  std::ostream& operator<<(std::ostream&, const TokenType);

  class Token
  {
  private:
    friend std::ostream &operator<<(std::ostream &str, const Token &token);

  protected:
    virtual std::ostream& print(std::ostream& out) const;

  public:
    Token(TokenType token_type, std::shared_ptr<Position> start = nullptr, std::shared_ptr<Position> end = nullptr);
    virtual ~Token();

    TokenType token_type;
    std::shared_ptr<Position> start;
    std::shared_ptr<Position> end;

  };

  class IntToken : public Token
  {
  public:
    int value;

    IntToken(int value, std::shared_ptr<Position> start = nullptr, std::shared_ptr<Position> end = nullptr);
    std::ostream& print(std::ostream& out) const override;
  };

  class FloatToken : public Token
  {
  public:
    double value;

    FloatToken(double value, std::shared_ptr<Position> start = nullptr, std::shared_ptr<Position> end = nullptr);
    std::ostream& print(std::ostream& out) const override;
  };

} // namespace Basic
