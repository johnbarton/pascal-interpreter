#pragma once

#include <memory>
#include <string>

namespace Basic {
 class Position {
   public:
    int index;
    int line;
    int column;
    std::string filename;
    std::string text;

    Position(int index, int line, int column, std::string filename, std::string text);
    void advance(char currentChar = (char) NULL);
    std::shared_ptr<Position> copy();
 };
}
