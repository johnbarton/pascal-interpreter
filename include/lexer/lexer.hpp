#pragma once

#include "lexer/position.hpp"
#include "lexer/token.hpp"

#include <vector>
#include <memory>
#include <string>

namespace Basic {
 class Lexer {
   private:
    std::string filename;
    std::string text;
    std::shared_ptr<Position> position;
    char current_char;

   public:
    Lexer(std::string filename, std::string text);
    void advance();
    std::vector<std::shared_ptr<Token>> makeTokens();
    std::shared_ptr<Token> makeNumber();
 };
}
