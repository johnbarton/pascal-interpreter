#pragma once

#include "lexer/token.hpp"
#include "parser/node.hpp"
// #include "interpreter/visitor.hpp"

#include <memory>

namespace Basic
{
  class UnaryOpNode : public Node//, public std::enable_shared_from_this<Node>
  {
  private:
    std::shared_ptr<Token> token;
    std::shared_ptr<Node> node;
    std::ostream &print(std::ostream &out) const override;

  public:
    UnaryOpNode(std::shared_ptr<Token> token, std::shared_ptr<Node> node);
    ~UnaryOpNode() override;
    // void accept(std::shared_ptr<AbstractVisitor>) override;
  };
} // namespace Basic