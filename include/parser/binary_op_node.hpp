#pragma once

#include "lexer/token.hpp"
#include "parser/node.hpp"
// #include "interpreter/visitor.hpp"

#include <memory>

namespace Basic
{
  class BinaryOpNode : public Node//, public std::enable_shared_from_this<Node>
  {
  private:
    std::shared_ptr<Node> left_node;
    std::shared_ptr<Token> token;
    std::shared_ptr<Node> right_node;

    std::ostream &print(std::ostream &out) const override;

  public:
    BinaryOpNode(std::shared_ptr<Node> left_node, std::shared_ptr<Token> token, std::shared_ptr<Node> right_node);
    ~BinaryOpNode() override;
    // void accept(std::shared_ptr<AbstractVisitor>) override;
    std::shared_ptr<Node> getLeftNode();
    std::shared_ptr<Token> getToken();
    std::shared_ptr<Node> getRightNode();
  };
} // namespace Basic