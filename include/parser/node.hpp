#pragma once

#include <memory>
#include <sstream>

namespace Basic
{
  class AbstractVisitor;

  class Node
  {
  protected:
    Node() = default;
    virtual ~Node();
    virtual std::ostream &print(std::ostream &out) const = 0;

  public:
    friend std::ostream &operator<<(std::ostream &str, const Node &node);
    // virtual void accept(std::shared_ptr<AbstractVisitor>) = 0;
  };
} // namespace Basic
