#pragma once

#include "lexer/token.hpp"
#include "parser/parse_result.hpp"

#include <memory>
#include <set>
#include <vector>

namespace Basic
{
  class Parser
  {
  private:
    std::vector<std::shared_ptr<Token>> tokens;
    std::shared_ptr<Token> current_token;
    int token_index;

    std::shared_ptr<Token> advance();
    std::shared_ptr<ParseResult> factor();
    std::shared_ptr<ParseResult> binary_op(std::shared_ptr<ParseResult>(Parser::*)(void), std::set<TokenType>);
    std::shared_ptr<ParseResult> term();
    std::shared_ptr<ParseResult> expression();

  public:
    Parser(std::vector<std::shared_ptr<Token>> tokens);

    std::shared_ptr<ParseResult> parse();
  };
} // namespace Basic