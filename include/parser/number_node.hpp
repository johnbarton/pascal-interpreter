#pragma once

#include "lexer/token.hpp"
#include "parser/node.hpp"
// #include "interpreter/visitor.hpp"

#include <memory>

namespace Basic
{
  class NumberNode : public Node//, public std::enable_shared_from_this<Node>
  {
  private:
    std::shared_ptr<Token> token;
    std::ostream& print(std::ostream&) const override;

  public:
    NumberNode(std::shared_ptr<Token> token);
    ~NumberNode() override;
    // void accept(std::shared_ptr<AbstractVisitor>) override;
  };
} // namespace Basic