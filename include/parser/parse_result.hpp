#pragma once

#include "core/error.hpp"
#include "parser/node.hpp"

#include <memory>

namespace Basic
{
  class ParseResult : public std::enable_shared_from_this<ParseResult>
  {
  private:
    std::shared_ptr<Node> node;

  public:
    ParseResult();

    std::shared_ptr<Node> record(std::shared_ptr<ParseResult>);
    std::shared_ptr<Node> record(std::shared_ptr<Node>);
    std::shared_ptr<ParseResult> success(std::shared_ptr<Node>);

    std::shared_ptr<Node> getNode();
  };
} // namespace Basic