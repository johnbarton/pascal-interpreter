#pragma once

#include "basic/basic.hpp"

namespace Basic {
 class Shell {
   protected:
    Basic basic;

   public:
    int run();
 };
}
