// #pragma once

// #include <memory>

// #include "parser/number_node.hpp"
// #include "parser/binary_op_node.hpp"
// #include "parser/unary_op_node.hpp"

// namespace Basic
// {
//   class NumberNode;
//   class BinaryOpNode;
//   class UnaryOpNode;

//   class AbstractVisitor
//   {
//   public:
//     virtual void visit(NumberNode* node) = 0;
//     virtual void visit(BinaryOpNode* node) = 0;
//     virtual void visit(UnaryOpNode* node) = 0;
//   };

//   class Visitor : public AbstractVisitor
//   {
//   public:
//     void visit(NumberNode*) override;
//     void visit(BinaryOpNode*) override;
//     void visit(UnaryOpNode*) override;
//   };
// } // namespace Basic
