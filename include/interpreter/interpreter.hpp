#pragma once

// #include "interpreter/visitor.hpp"
#include "parser/node.hpp"

#include <memory>

namespace Basic
{
  class Interpreter
  {
  private:
    // Visitor visitor;

  public:
    Interpreter();
    void visit(std::shared_ptr<Node> node);
  };
} // namespace Basic