#include "interpreter/interpreter.hpp"

#include <iostream>

namespace Basic {
  //helper function to test what type of node it is - I hate this
  template<typename B, typename T>
  inline bool instanceof(const T*) {
    return std::is_base_of<B, T>::value;
  }


  Interpreter::Interpreter() {
    //
  }

  void Interpreter::visit(std::shared_ptr<Node> node) {
    // node->accept(this->visitor);
    if (instanceof<BinaryOpNode>(node.get())) {

    } else if (instanceof<NumberNode>(node.get()) {

    } else if (instanceof<UnaryNode>(node.get()))
  }
}