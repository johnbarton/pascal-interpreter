#include "shell/shell.hpp"

int main(int argc, char** argv) {
  Basic::Shell shell;
  return shell.run();
}
