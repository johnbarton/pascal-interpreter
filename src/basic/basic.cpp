#include "basic/basic.hpp"
#include "core/error.hpp"
#include "lexer/lexer.hpp"
#include "parser/parser.hpp"
#include "interpreter/interpreter.hpp"

#include <iostream>
#include <memory>
#include <string>

namespace Basic {

std::shared_ptr<Node> Basic::run(std::string filename, std::string text) {
  Lexer lexer(filename, text);
  auto tokens = lexer.makeTokens();

  Parser parser(tokens);
  auto ast = parser.parse();

  Interpreter interpreter;
  interpreter.visit(ast->getNode());

  return ast->getNode();
}


}