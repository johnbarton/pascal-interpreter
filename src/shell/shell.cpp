#include "shell/shell.hpp"
#include "core/error.hpp"

#include <iostream>
#include <memory>
#include <string>

namespace Basic
{

  int Shell::run()
  {
    while (true)
    {
      try
      {
        std::string expression = "(1 + 3) * 6";
        std::cout << "basic > ";
        std::getline(std::cin, expression);

        if (expression.compare("exit") == 0)
        {
          break;
        }

        std::shared_ptr<Node> node = basic.run("<stdin>", expression);
        std::cout << *node << std::endl;
        // std::cout << node.use_count() << std::endl;
      }
      catch (const Error &e)
      {
        std::cerr << e.what() << std::endl;
      }
      catch (const std::exception &e)
      {
        std::cerr << "Unhandled exception: " << e.what() << std::endl;
        throw e;
        return -1;
      }
    }

    return 0;
  }

} // namespace Basic