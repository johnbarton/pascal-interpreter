#include "lexer/position.hpp"

namespace Basic {

  Position::Position(int index, int line, int column, std::string filename, std::string text) :
    index(index), line(line), column(column), filename(filename), text(text)
  {
    //
  }

  void Position::advance(char currentChar) {
    this->index++;
    this->column++;

    if (currentChar == '\n') {
      this->line++;
      this->column = 0;
    }

    // return this;
  }

  std::shared_ptr<Position> Position::copy() {
    return std::make_shared<Position>(this->index, this->line, this->column, this->filename, this->text);
  }
}