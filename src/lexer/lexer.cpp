#include "lexer/lexer.hpp"
#include "core/error.hpp"

#include <cctype>
#include <iostream>
#include <memory>

namespace Basic {

  Lexer::Lexer(std::string filename, std::string text) :
    filename(filename),
    text(text),
    position(std::make_shared<Position>(-1, 0, -1, filename, text)),
    current_char((char) NULL)
  {
    this->advance();
  }

  void Lexer::advance() {
    this->position->advance(this->current_char);

    if (this->position->index < this->text.length()) {
      this->current_char = this->text[this->position->index];
    } else {
      this->current_char = (char) NULL;
    }
  }

  std::vector<std::shared_ptr<Token>> Lexer::makeTokens() {
    std::vector<std::shared_ptr<Token>> tokens;

    while (this->current_char != (char) NULL) {
      // Skip spaces
      if (this->current_char == ' ' || this->current_char == '\t') {
        this->advance();
      } else if (isdigit(this->current_char)) {
        tokens.emplace_back(this->makeNumber());
      } else {
        switch (this->current_char) {
        case '+':
          tokens.emplace_back(std::make_shared<Token>(TT_PLUS, this->position));
          this->advance();
          break;
        case '-':
          tokens.emplace_back(std::make_shared<Token>(TT_MINUS, this->position));
          this->advance();
          break;
        case '*':
          tokens.emplace_back(std::make_shared<Token>(TT_MUL, this->position));
          this->advance();
          break;
        case '/':
          tokens.emplace_back(std::make_shared<Token>(TT_DIV, this->position));
          this->advance();
          break;
        case '(':
          tokens.emplace_back(std::make_shared<Token>(TT_LPAREN, this->position));
          this->advance();
          break;
        case ')':
          tokens.emplace_back(std::make_shared<Token>(TT_RPAREN, this->position));
          this->advance();
          break;
        default:
          // std::cout << "ahh" << std::endl;
          auto start = this->position->copy();
          std::string current_char_str(1, this->current_char);
          this->advance();
          throw IllegalCharError(start, this->position, "'" + current_char_str + "'");
        }
      }
    }

    tokens.emplace_back(std::make_shared<Token>(TT_EOF, this->position));
    return tokens;
  }

  std::shared_ptr<Token> Lexer::makeNumber() {
    std::string number_str;
    bool has_decimal = false;
    std::shared_ptr<Position> start = this->position->copy();

    while (this->current_char != (char) NULL && (isdigit(this->current_char) || this->current_char == '.')) {
      if (current_char == '.') {
        if (has_decimal) {
          break;
        }

        has_decimal = true;
        number_str.append(".");
      } else {
        number_str.append(std::string(1, this->current_char));
      }

      this->advance();
    }

    if (has_decimal) {
      return std::make_shared<FloatToken>(std::stod(number_str), start, this->position);
    } else {
      return std::make_shared<IntToken>(std::stoi(number_str), start, this->position);
    }
  }

}