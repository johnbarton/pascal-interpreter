#include "lexer/token.hpp"

#include <iostream>
#include <map>

namespace Basic {
  Token::Token(TokenType token_type, std::shared_ptr<Position> start, std::shared_ptr<Position> end) :
    token_type(token_type)
  {
    if (start != nullptr) {
      this->start = start->copy();
      this->end = start->copy();
      this->end->advance();
    }

    if (end != nullptr) {
      this->end = end;
    }
  }

  Token::~Token() {
    //
  }

  IntToken::IntToken(int value, std::shared_ptr<Position> start, std::shared_ptr<Position> end) :
    Token(TT_INT, start, end), value(value)
  {
    //
  }

  FloatToken::FloatToken(double value, std::shared_ptr<Position> start, std::shared_ptr<Position> end) :
    Token(TT_FLOAT, start, end), value(value)
  {
    //
  }

  std::ostream& Token::print(std::ostream& out) const {
    out << this->token_type;
    return out;
  }

  std::ostream& IntToken::print(std::ostream& out) const {
    out << this->token_type << ":" << this->value;
    return out;
  }

  std::ostream& FloatToken::print(std::ostream& out) const {
    out << this->token_type << ":" << this->value;
    return out;
  }

  std::ostream& operator<<(std::ostream& out, const TokenType value) {
    static std::map<TokenType, std::string> strings;

      if (strings.size() == 0){
#define INSERT_ELEMENT(p) strings[p] = #p
          INSERT_ELEMENT(TT_INT);
          INSERT_ELEMENT(TT_FLOAT);
          INSERT_ELEMENT(TT_PLUS);
          INSERT_ELEMENT(TT_MINUS);
          INSERT_ELEMENT(TT_MUL);
          INSERT_ELEMENT(TT_DIV);
          INSERT_ELEMENT(TT_LPAREN);
          INSERT_ELEMENT(TT_RPAREN);
          INSERT_ELEMENT(TT_EOF);
#undef INSERT_ELEMENT
      }

    return out << strings[value];
  }

  std::ostream &operator<<(std::ostream &str, const Token &token) {
    return token.print(str);
  }

}