#include "core/error.hpp"

#include <algorithm>
#include <iostream>

namespace Basic {
  Error::Error(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string name, std::string details) :
    start{start}, end{end}, name{name}, details{details}
  {
    //
  }

  // const char* string_with_arrows(std::string text, std::shared_ptr<Position> start, std::shared_ptr<Position> end) {
  //   std::ostringstream result;

  //   // Calculate indices
  //   int ind_start = std::max((int) text.rfind("\n", 0, start->index), 0);

  // }

  const char* Error::what() const throw() {
    std::ostringstream out;
    out << this->name << ": " << this->details << std::endl;
    out << "File " << this->start->filename <<  ", Line " << this->start->line + 1 << std::endl;
    // out << std::endl << std::endl << string_with_arrows(this->start->text, this->start, this->end);
    std::cout << out.str().c_str();
    return out.str().c_str();
  }

  IllegalCharError::IllegalCharError(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string details)
    : Error(start, end, "Illegal Character", details)
  {
    //
  }

  InvalidSyntaxError::InvalidSyntaxError(std::shared_ptr<Position> start, std::shared_ptr<Position> end, std::string details)
    : Error(start, end, "Invalid Syntax", details)
  {
    //
  }
}