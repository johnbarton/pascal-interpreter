#include "parser/parser.hpp"
#include "parser/parse_result.hpp"
#include "parser/node.hpp"
#include "parser/number_node.hpp"
#include "parser/unary_op_node.hpp"
#include "parser/binary_op_node.hpp"
#include "core/error.hpp"

namespace Basic
{
  Parser::Parser(std::vector<std::shared_ptr<Token>> tokens) : tokens(tokens), token_index(-1)
  {
    this->advance();
  }

  std::shared_ptr<ParseResult> Parser::parse()
  {
    auto parsed = this->expression();
    if (this->current_token->token_type != TT_EOF)
    {
      throw InvalidSyntaxError(this->current_token->start, this->current_token->end, "Expected '+', '-', '*' or '/'");
    }
    return parsed;
  }

  std::shared_ptr<Token> Parser::advance()
  {
    this->token_index++;
    if (this->token_index < this->tokens.size())
    {
      this->current_token = this->tokens[this->token_index];
    }
    return this->current_token;
  }

  std::shared_ptr<ParseResult> Parser::factor()
  {
    auto result = std::make_shared<ParseResult>();
    auto token = this->current_token;

    if (token->token_type == TT_PLUS || token->token_type == TT_MINUS)
    {
      this->advance();
      auto factor = result->record(this->factor());
      return result->success(std::make_shared<UnaryOpNode>(token, factor));
    }
    else if (token->token_type == TT_INT || token->token_type == TT_FLOAT)
    {
      this->advance();
      return result->success(std::make_shared<NumberNode>(token));
    }
    else if (token->token_type == TT_LPAREN)
    {
      this->advance();
      auto expression = result->record(this->expression());
      if (this->current_token->token_type == TT_RPAREN)
      {
        this->advance();
        return result->success(expression);
      }
      else
      {
        throw InvalidSyntaxError(this->current_token->start, this->current_token->end, "Expected ')'");
      }
    }
    throw IllegalCharError(token->start, token->end, "Expected int or float");
  }

  std::shared_ptr<ParseResult> Parser::binary_op(std::shared_ptr<ParseResult> (Parser::*function)(void), std::set<TokenType> token_types)
  {
    auto result = std::make_shared<ParseResult>();
    auto left = result->record((this->*function)());

    while (token_types.find(this->current_token->token_type) != token_types.end())
    {
      auto token = this->current_token;
      this->advance();
      auto right = result->record((this->*function)());
      left = std::make_shared<BinaryOpNode>(left, token, right);
    }
    return result->success(left);
  }

  std::shared_ptr<ParseResult> Parser::term()
  {
    return this->binary_op(&Parser::factor, {TT_MUL, TT_DIV});
  }

  std::shared_ptr<ParseResult> Parser::expression()
  {
    return this->binary_op(&Parser::term, {TT_PLUS, TT_MINUS});
  }
} // namespace Basic