#include "parser/parse_result.hpp"

namespace Basic {

  ParseResult::ParseResult() : node(nullptr)
  {
    //
  }

  std::shared_ptr<Node> ParseResult::record(std::shared_ptr<ParseResult> result) {
    return result->node;
  }

  std::shared_ptr<Node> ParseResult::record(std::shared_ptr<Node> node) {
    return node;
  }

  std::shared_ptr<ParseResult> ParseResult::success(std::shared_ptr<Node> node) {
    this->node = node;
    return shared_from_this();
  }

  std::shared_ptr<Node> ParseResult::getNode() {
    return this->node;
  }
}