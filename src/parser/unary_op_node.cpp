#include "parser/unary_op_node.hpp"

namespace Basic
{
  UnaryOpNode::UnaryOpNode(std::shared_ptr<Token> token, std::shared_ptr<Node> node) : token(token), node(node)
  {
    //
  }

  UnaryOpNode::~UnaryOpNode()
  {
    //
  }

  std::ostream &UnaryOpNode::print(std::ostream &out) const {
    out << "(" << *this->token << ", " << *this->node << ")";
    return out;
  }

  // void UnaryOpNode::accept(std::shared_ptr<AbstractVisitor> visitor) {
  //   visitor->visit(this);
  // }
} // namespace Basic