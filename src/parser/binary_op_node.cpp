#include "parser/binary_op_node.hpp"

namespace Basic {

  BinaryOpNode::BinaryOpNode(std::shared_ptr<Node> left_node, std::shared_ptr<Token> token, std::shared_ptr<Node> right_node) :
    left_node(left_node), token(token), right_node(right_node)
  {
    //
  }

  BinaryOpNode::~BinaryOpNode()
  {
    //
  }

  std::ostream &BinaryOpNode::print(std::ostream &out) const {
    out << "(" << *this->left_node << ", " << *this->token << ", " << *this->right_node << ")";
    return out;
  }

  // void BinaryOpNode::accept(std::shared_ptr<AbstractVisitor> visitor) {
  //   visitor->visit(this);
  // }


  std::shared_ptr<Node> BinaryOpNode::getLeftNode() {
    return this->left_node;
  }

  std::shared_ptr<Token> BinaryOpNode::getToken() {
    return this->token;
  }

  std::shared_ptr<Node> BinaryOpNode::getRightNode() {
    return this->right_node;
  }

}
