#include "parser/number_node.hpp"

namespace Basic
{
  NumberNode::NumberNode(std::shared_ptr<Token> token) : token(token)
  {
    //
  }

  NumberNode::~NumberNode()
  {
    //
  }

  std::ostream& NumberNode::print(std::ostream& out) const {
    out << *this->token;
    return out;
  }

  // void NumberNode::accept(std::shared_ptr<AbstractVisitor> visitor) {
  //   visitor->visit(this);
  // }

} // namespace Basic