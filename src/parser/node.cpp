#include "parser/node.hpp"

namespace Basic
{

  Node::~Node()
  {
    //
  }

  std::ostream &operator<<(std::ostream &str, const Node &node) {
    return node.print(str);
  }

} // namespace Basic